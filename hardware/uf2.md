## 10 nov 2020
```
[root@a16 ~]# lsblk -o NAME,MODEL,FSTYPE,SIZE,TYPE,MOUNTPOINT
NAME   MODEL             FSTYPE   SIZE TYPE MOUNTPOINT
sda    ST500DM002-1BD142        465.8G disk 
├─sda1                              1K part 
├─sda5                   ext4     100G part /
├─sda6                   ext4     100G part 
└─sda7                   swap       5G part [SWAP]
[root@a16 ~]# lsblk -S
NAME HCTL       TYPE VENDOR   MODEL              REV TRAN
sda  1:0:0:0    disk ATA      ST500DM002-1BD142 KC48 sata
[root@a16 ~]# 

alias per crear comandes en columnes que son de major utilitat com lsblk -o NAME,MODEL,FSTYPE,SIZE,TYPE,MOUNTPOINT convertir-ho a 'lsdisk'
[root@a16 ~]# alias lsdisk='lsblk -o NAME,MODEL,FSTYPE,SIZE,TYPE,MOUNTPOINT'
[root@a16 ~]# lsdisk
NAME   MODEL             FSTYPE   SIZE TYPE MOUNTPOINT
sda    ST500DM002-1BD142        465.8G disk 
├─sda1                              1K part 
├─sda5                   ext4     100G part /
├─sda6                   ext4     100G part 
└─sda7                   swap       5G part [SWAP]
[root@a16 ~]# 
comandes per veure quant espai queda al disc dur
[root@a16 ~]# df -h
Filesystem      Size  Used Avail Use% Mounted on
devtmpfs        3.9G     0  3.9G   0% /dev
tmpfs           3.9G  209M  3.7G   6% /dev/shm
tmpfs           3.9G  1.7M  3.9G   1% /run
/dev/sda5        98G   12G   82G  12% /
tmpfs           3.9G  216K  3.9G   1% /tmp
tmpfs           785M  104K  785M   1% /run/user/1000
[root@a16 ~]# 
```
rm -f /root/prova2G; date; cp /mnt/prova2G; date