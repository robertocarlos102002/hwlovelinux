## 1. Un linux que faci servir una partició de grub, una de swap i una per la resta del sistema de fitxers
```
parted -s  /dev/vda mklabel msdos
parted -s /dev/vda mkpart primary 1MB 1GB
parted -s /dev/vda mkpart primary 1GB 9GB
parted -s /dev/vda mkpart extended 9GB 100%
parted -s /dev/vda mkpart logical 9GB 11GB
parted -s /dev/vda mkpart logical 11GB 100%

```
## 2. Un linux que faci servir una partició de grub, una de swap, una per la home i una per la resta del sistema de fitxers
```
parted -s  /dev/vda mklabel msdos
parted -s /dev/vda mkpart primary 1MB 1GB
parted -s /dev/vda mkpart primary 1GB 9GB
parted -s /dev/vda mkpart extended 9GB 100%
parted -s /dev/vda mkpart logical 9GB 13GB
parted -s /dev/vda mkpart logical 13GB 15GB
parted -s /dev/vda mkpart logical 15GB 100%
```
## 3. Un windows amb una partició de dades i un altre pel sistema operatiu
```
parted -s /dev/vda mklabel msdos
parted -s /dev/vda mkpart primary 1MB 7GB
parted -s /dev/vda mkpart extended 7GB 100%
parted -s /dev/vda mkpart logical 7GB 100%
```
## 4. Un windows amb una partició de dades, una partició pel sistema operatiu i una partició per fer un backup
```
parted -s /dev/vda mklabel msdos
parted -s /dev/vda mkpart primary 1MB 7GB
parted -s /dev/vda mkpart extended 8GB 100%
parted -s /dev/vda mkpart logical 7GB 15GB
parted -s /dev/vda mkpart logical 15GB 100%
```
## 5. Un arranc dual d'un windows i un linux 
```
parted -s /dev/vda mklabel msdos
parted -s /dev/vda mkpart primary 1MB 1GB
parted -s /dev/vda mkpart primary 1GB 8GB
parted -s /dev/vda mkpart primary 8GB 100%
``` 
